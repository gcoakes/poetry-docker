{ pkgs ? import <nixpkgs> {} }:
let
  dockerImg = ps: tag: pkgs.dockerTools.buildImage {
    name = "gcoakes/poetry";
    tag = tag;
    contents = with pkgs; [
      cacert
      bashInteractive
      coreutils
      findutils
      ps.poetry
      gnumake
    ];
    runAsRoot = ''
      #!${pkgs.stdenv.shell}
      mkdir -p /usr/bin
      ln -s /bin/env /usr/bin/env
    '';
    config.Cmd = [ "${pkgs.bashInteractive}/bin/bash" ];
  };
in {
  poetry27 = dockerImg pkgs.python27Packages "2.7";
  poetry38 = dockerImg pkgs.python38Packages "3.8";
  poetryLatest = dockerImg pkgs.python38Packages "latest";
}
