.PHONY: update
update:
	pass apppass/hub.docker.com | docker login --username=gcoakes --password-stdin=true
	docker load <$$(nix-build poetry.nix -A poetry27 --no-out-link)
	docker push gcoakes/poetry:2.7
	docker load <$$(nix-build poetry.nix -A poetry38 --no-out-link)
	docker push gcoakes/poetry:3.8
	docker load <$$(nix-build poetry.nix -A poetryLatest --no-out-link)
	docker push gcoakes/poetry:latest
